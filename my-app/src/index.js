import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom'
import App from './App.js';
import CssBaseline from '@material-ui/core/CssBaseline';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  (
    <React.Fragment>
      <CssBaseline/>
      <BrowserRouter>
        <App/>
      </BrowserRouter>
    </React.Fragment>
  ),
  document.getElementById('root')
);
registerServiceWorker();
