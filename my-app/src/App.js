import React, { Component, Fragment } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Example from './example';
import 'typeface-poppins';

const palettes = {
  octopus: {
    primary: '#000000',
    highlightedPrimary: '#E1423C',
    secondary: '#F47265',
    highlightedSecondary: '#FCBA97',
    tertiary: '#5E3A3A',
    highlightedTertiary: '#7E3D41',
    text: '#E38C95',
    highlightedText: '#db6c78',
    background1: '#F1E0D8',
    background2: '#f6ebe6',
  },
}

const theme = createMuiTheme({
  colourPalette: palettes.octopus,
  typography: {
    fontFamily:'"Poppins",sans-serif',
  },
})

let timeout = 0

class App extends Component {
  state = {
    width: window.innerWidth,
    height: window.innerHeight,
    userPlat: null,
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.resizeControl);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.resizeControl);
  }

  resizeControl = () => {
    timeout = timeout + 1
    let hold = timeout
    setTimeout(() => {
      if (hold === timeout) {
        this.updateDimensions()
      }
    }, 20)
  }

  updateDimensions = () => {
    let state = this.state;
    state.width = window.innerWidth;
    state.height = window.innerHeight;
    let userPlat
    if (state.width < 500) {userPlat = 'mobile'}
    if (state.width >= 500 && state.width < 1000 && state.width < state.height) {userPlat = 'padVert'}
    if (state.width >= 500 && state.width < 1000 && state.width > state.height) {userPlat = 'padHori'}
    if (state.width >= 1000) {userPlat = 'pc'}
    if (state.width >= 2000) {userPlat = 'largePc'}
    state.userPlat = userPlat
    this.setState(state)
  }

  render() {
    return (
      <Fragment>
        <MuiThemeProvider theme={theme} >
          <Example 
            dimensions={{
              height: this.state.height,
              width: this.state.width,
              userPlat: this.state.userPlat
            }}
          />
        </MuiThemeProvider>
      </Fragment>
    )
  }
}

export default App;
