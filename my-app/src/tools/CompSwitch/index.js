import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DivAnimation from '../DivAnimation'

class CompSwitch extends Component {
  state = {
    backup: () => {},
    control: true,
  };

  componentWillMount() {
    this.setState({backup: this.props.children})
  };

  componentWillReceiveProps(nextProps) {
    if (
      this.props.precise !== null || 
      this.props.change !== nextProps.change
    ) {
      this.setState({control: false})
    };
  };

  handleControl = (duration) => {
    const delay = duration / 2;
    setTimeout(() => {
      this.setState({
        backup: this.props.children,
        control: true
      })
    }, delay + 50)
  };

  render() {

    const { delay, children, full } = this.props;
    const { backup, control } = this.state;

    !control && this.handleControl(delay)

    return (
      <React.Fragment>
        <DivAnimation
          entering={{opacity: 0}}
          entered={{opacity: 1}}
          style={
            full
              ? {opacity: 0, height: '100%', width: '100%'}
              : {opacity: 0}
          }
          transition={delay / 2 + "ms ease-in-out"}
          toggle={control}
        >
          {!control
            ? backup
            : children
          }
        </DivAnimation>
      </React.Fragment>
    )
  }
}

CompSwitch.defaultProps = {
  delay: 400,
  change: '',
  precise: null,
};

CompSwitch.propTypes = {
  delay: PropTypes.number,
  change: PropTypes.any.isRequired,
  precise: PropTypes.any,
};

export default CompSwitch;