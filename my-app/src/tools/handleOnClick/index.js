import React, { Component, Fragment } from 'react';

let timer;

class HandleOnClick extends Component {
	
	initialise = (persistantClick) => {
		timer = 1
		setTimeout(() => {
			if (timer === 1) {
				persistantClick()
				timer = 0
			}
		}, 150)
	}

	intervene = (singleClick, suspendedClick) => {
		if (timer === 1)
			singleClick()
		else
			suspendedClick && suspendedClick()
		timer = 0;

	}

	render(){

		const { children, singleClick, persistantClick, suspendedClick } = this.props;

		return(
			<div
				style={{height: '100%', width: '100%'}}
				onMouseDown={() => this.initialise(persistantClick)}
				onMouseUp={() => {
					this.intervene(singleClick, suspendedClick && suspendedClick)
				}}
			>
				{children}
			</div>
		)
	}
}

export default HandleOnClick;