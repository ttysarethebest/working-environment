import React from 'react';
import Transition from 'react-transition-group/Transition';

// props:
// entering: object
// entered: object
// style : object
// transition: string
// appear: bool
// toggle: bool
// children: JSX element

const DivAnimation = (props) => {
  DivAnimation.defaultProps = {
    entering: {},
    entered: {},
    style: {},
    transition: '',
    appear: false,
    toggle: true,
    children: "empty",
  };

  const transitionStyles = {
    entering: props.entering,
    entered: props.entered
  };

  const defaultStyle = props.style;

  if (props.style) {
    defaultStyle['transition'] = props.transition
  }

  return (
    <React.Fragment>
      <Transition
        appear={props.appear}
        in={props.toggle}
        timeout={0}
      >
        {(state) => (
          <div
            style={{
              ...defaultStyle,
              ...transitionStyles[state]
            }}
          >
            {props.children}
          </div>
        )}
      </Transition>
    </React.Fragment>
  )
};

export default DivAnimation;