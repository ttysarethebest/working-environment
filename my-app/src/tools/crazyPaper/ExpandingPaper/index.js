import React, { Component, Fragment } from 'react';

class ExpandingPaper extends Component {

	constructor(props) {
	  super(props);
	  this.animate = true
	}

	componentWillUpdate(nextProps) {
		this.props.dimensions !== nextProps.dimensions
			? this.animate = false
			: this.animate = true
	}

	shouldComponentUpdate(nextProps) {
		return(this.props !== nextProps
			? true
			: false
		)
	}

	render() {

		const { story, position, children } = this.props

		// Props and Usage:
		// story: and array of objects containing: {height: % etc., width: %etc., t: int, tF: string(eg linear), delay: int }
		// position: selected index of array
		// children: standard children prop
		// Usage: pass a story and select which index you would like to show, transitions happen automatically

		const spec = {
				height: story[position].height,
				width: story[position].width,
		  	transition: 'height ' + story[position].t + 's, width ' + story[position].t + 's',
		  	transitionTimingFunction: story[position].tF ? story[position].tF : 'linear',
		  	transitionDelay: story[position].delay ? story[position].delay + 's' : 0 + 's',
		}

		const reposition = {
			height: story[position].height,
			width: story[position].width,
		}

		return (
			<div style={this.animate ? spec : reposition} >
				{children}
			</div>
		)
	}
}

export default ExpandingPaper;