import React, { Component, Fragment } from 'react';

class MovingPaper extends Component {

	constructor(props) {
	  super(props);
	  this.animate = true
	}

	componentWillUpdate(nextProps) {
		this.props.dimensions !== nextProps.dimensions
			? this.animate = false
			: this.animate = true
	}

	shouldComponentUpdate(nextProps) {
		return(this.props !== nextProps
			? true
			: false
		)
	}

	render() {

		const { story, position, children } = this.props;

		// Props and Usage:
		// story: and array of objects containing: {x: % etc., y: %etc., t: int, tF: string(eg linear), delay: int }
		// position: selected index of array
		// children: standard children prop
		// Usage: pass a story and select which index you would like to show, transitions happen automatically

		const spec = {
		  	position: 'absolute',
		  	left: story[position].x,
		  	top: story[position].y,
		  	transition: 'left ' + story[position].t + 's, top ' + story[position].t + 's',
		  	transitionTimingFunction: story[position].tF ? story[position].tF : 'linear',
		  	transitionDelay: story[position].delay ? story[position].delay + 's' : 0 + 's',
		}

		const reposition = {
			position: 'absolute',
		  	left: story[position].x,
		  	top: story[position].y,
		}

		return (
			<Fragment>
					<div style={this.animate ? spec : reposition} >
						{children}
					</div>
			</Fragment>
		)
	}
}

export default MovingPaper;