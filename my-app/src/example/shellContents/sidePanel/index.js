import React, { Component, Fragment } from 'react';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import SideNavigation from '../../components/SideNav'

const styles = theme => ({
  paperClosed: {
    width: '100%',
    height: '100%',
    borderRadius: '25px',
    transition: 'border-radius 0.1s',
    transitionTimingFunction: 'ease-in-out',
  },
  paperOpen: {
    width: '100%',
    height: '100%',
    borderRadius: '0px',
    transition: 'border-radius 0.1s',
    transitionTimingFunction: 'ease-in-out',
  } 
})

class SidePanelContents extends Component {


  handelOnClick = () => {

    const { app, setter } = this.props;

    if (app.sidePanel === 'open'){
      setter({parent: 'app', target: 'sidePanel', value: 'expanded'}, 'sidePanel Contents')
      setter({parent: 'app', target: 'visual', value: 'expanded'}, 'sidePanel Contents')
    }
    else if (app.sidePanel === 'expanded'){
      setter({parent: 'app', target: 'sidePanel', value: 'open'}, 'sidePanel Contents')
      setter({parent: 'app', target: 'visual', value: 'open'}, 'sidePanel Contents')
    }
  }

  render() {

    const { setter, app, classes, story, menuItems } = this.props

    return (
      <Paper
        onClick={() => this.handelOnClick()}
        className={
          (story.sidePanel.size === 0)
            ? classes.paperClosed
            : classes.paperOpen
        }
      >
        <SideNavigation
          menuItems={menuItems}
          displayedContents={"Home"}
          app={app}
          update={(story.sidePanel.size > 1)}
        />
      </Paper>
    )
  }
}

export default withStyles(styles)(SidePanelContents);