import React, { Component, Fragment } from 'react';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import HandleOnClick from '../../../tools/handleOnClick'

const styles = theme => ({
  paper: {
    width: '100%',
    height: '100%',
    borderRadius: '25px'
  }
})

class TopPanelContents extends Component {

  handleClick = () => {
    
    const { app, setter } = this.props;

    if (app.topPanel === 'open')
      setter({parent: 'app', target: 'topPanel', value: 'closed'}, 'TopPanelContents')
    else if (app.topPanel === 'closed')
      setter({parent: 'app', target: 'topPanel', value: 'open'}, 'TopPanelContents')
  }

  render() {

    const { setter, app, classes } = this.props


    return (
      <HandleOnClick
        singleClick={() => this.handleClick()}
        persistantClick={() => app.topPanel === 'closed' && setter({parent: 'app', target: 'topPanel', value: 'minorOpen'}, 'TopPanelContents')}
        suspendedClick={() => setter({parent: 'app', target: 'topPanel', value: 'closed'}, 'TopPanelContents')}
      >
        <Paper
          className={classes.paper}
        >
        </Paper>
      </HandleOnClick>
    )
  }
}

export default withStyles(styles)(TopPanelContents)