import React, { Component, Fragment } from 'react';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import HandleOnClick from '../../../tools/handleOnClick'

const styles = theme => ({
  paperClosed: {
    width: '100%',
    height: '100%',
    borderRadius: '25px',
    transition: 'border-radius 0.1s',
    transitionTimingFunction: 'ease-in-out',
  },
  paperOpen: {
    width: '100%',
    height: '100%',
    borderRadius: '0px',
    transition: 'border-radius 0.1s',
    transitionTimingFunction: 'ease-in-out',
  } 
})

class VisualContents extends Component {

  toggleVisual = () => {

    const { setter, app } = this.props

    if (app.visual === 'closed' && app.tstat === false) {
      setter({parent: 'app', target: 'tstat', value: true}, 'VisualContents')
      setTimeout(() => setter({parent: 'app', target: 'tstat', value: false}, 'VisualContents'), 800)
      setter({parent: 'app', target: 'visual', value: 'open'}, 'VisualContents')
      setter({parent: 'app', target: 'sidePanel', value: 'open'}, 'VisualContents')
      setter({parent: 'app', target: 'topPanel', value: 'open'}, 'VisualContents')
    }
    else if (app.tstat === false) {
      setter({parent: 'app', target: 'tstat', value: true}, 'VisualContents')
      setTimeout(() => setter({parent: 'app', target: 'tstat', value: false}, 'VisualContents'), 800)
      setter({parent: 'app', target: 'visual', value: 'closed'}, 'VisualContents')
      setter({parent: 'app', target: 'sidePanel', value: 'closed'}, 'VisualContents')
      setter({parent: 'app', target: 'topPanel', value: 'closed'}, 'VisualContents')
    }
  }

  render() {

    const { setter, app, classes, story } = this.props


    return (
      <HandleOnClick
        singleClick={() => this.toggleVisual()}
        persistantClick={() => console.log('held down')}
      >
        <Paper
          className={
            (story.visual.size === 0)
              ? classes.paperClosed
              : classes.paperOpen
          }
        >
        </Paper>
      </HandleOnClick>
    )
  }
}

export default withStyles(styles)(VisualContents);