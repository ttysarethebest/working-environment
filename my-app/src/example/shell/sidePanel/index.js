import React, { Component, Fragment } from 'react';
import MovingPaper from '../../../tools/crazyPaper/MovingPaper';
import ExpandingPaper from '../../../tools/crazyPaper/ExpandingPaper';
import Button from '@material-ui/core/Button';

class SidePanel extends Component {

	constructor(props) {
	  super(props);
	  this.update = null
	  this.status = null
	}

	componentDidMount() {
		const { app } = this.props;
		this.status = app.sidePanel
	}

	shouldComponentUpdate(nextProps) {
		return( this.props !== nextProps
			? true
			: false
		)
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.update !== nextProps.update) {
			this.update = nextProps.update
		}
	}

	spinEngine = () => {

		const { storySpinner, app } = this.props;

		const formStates = {
			closed:{
				open: [
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'move', value: 2}, timeout: 0},
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'size', value: 1}, timeout: 0},
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'size', value: 2}, timeout: 400},
				],
				expanded: [],
				hidden: [
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'move', value: 0}, timeout: 0},
				],
			},
			expanded: {
				closed: [
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'size', value: 1}, timeout: 0},
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'size', value: 0}, timeout: 400},
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'move', value: 1}, timeout: 800},
				],
				open: [
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'size', value: 2}, timeout: 0}
				],
			},
			open: {
				closed: [
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'size', value: 1}, timeout: 0},
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'size', value: 0}, timeout: 400},
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'move', value: 1}, timeout: 800},
				],
				expanded: [
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'size', value: 3}, timeout: 0},
				],
			},
			hidden: {
				closed: [
					{payload: {parent: 'story', target: 'sidePanel', subTarget: 'move', value: 1}, timeout: 0},
				]
			}
		}

		storySpinner(formStates[this.status][this.update])
		this.status = this.update


	}

	render() {

		const { story, classes, app, storySpinner, dimensions, children } = this.props

		const formHome = {
			move : [
				{x: '20px', y: '85px', t: 0.2, tF: 'ease-in-out'},
				{x: '10px', y: '85px', t: 0.2, tF: 'ease-in-out'},
				{x: '0px', y: '80px', t: 0.2, tF: 'ease-in-out'},
			],
			size: [
				{height: '40px', width: '40px', t: 0.4, tF: 'ease-in-out'},
				{height: dimensions.height -80, width: '40px', t: 0.4, tF: 'ease-in-out'},
				{height: dimensions.height -80, width: '60px', t: 0.4, tF: 'ease'},
				{height: dimensions.height -80, width: '240px', t: 0.4, tF: 'ease'},
			],
		}

		this.update !== null && this.spinEngine()
		this.update = null

		return (
			<Fragment>
				<MovingPaper
					story={formHome.move}
					position={story.sidePanel.move}
					dimensions={dimensions}
				>
	        <ExpandingPaper
	        	story={formHome.size}
	        	position={story.sidePanel.size}
	        	dimensions={dimensions}
	        >
	          {children}
	        </ExpandingPaper>
	      </MovingPaper>
	    </Fragment>
		)
	}
}

export default SidePanel