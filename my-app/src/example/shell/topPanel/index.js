import React, { Component, Fragment } from 'react';
import MovingPaper from '../../../tools/crazyPaper/MovingPaper';
import ExpandingPaper from '../../../tools/crazyPaper/ExpandingPaper';
import Button from '@material-ui/core/Button';

class TopPanel extends Component {

	constructor(props) {
	  super(props);
	  this.update = null
	  this.status = null
	}

	componentDidMount() {
		const { app } = this.props;
		this.status = app.topPanel;
	}

	shouldComponentUpdate(nextProps) {
		return( this.props !== nextProps
			? true
			: false
		)
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.update !== nextProps.update) {
			this.update = nextProps.update
		}
	}

	spinEngine = () => {

		const { storySpinner, app } = this.props;

		const formStates = {
			open: {
				closed: [
					{payload: {parent: 'story', target: 'topPanel', subTarget: 'size', value: 0}, timeout: 0},
				],
			},
			closed: {
				open: [
					{payload: {parent: 'story', target: 'topPanel', subTarget: 'size', value: 1}, timeout: 0},
				],
				minorOpen: [
					{payload: {parent: 'story', target: 'topPanel', subTarget: 'size', value: 2}, timeout: 0},
				],
				hidden: [
					{payload: {parent: 'story', target: 'topPanel', subTarget: 'move', value: 1}, timeout: 0}
				]
			},
			minorOpen: {
				closed: [
					{payload: {parent: 'story', target: 'topPanel', subTarget: 'size', value: 0}, timeout: 0},
				],
			},
			hidden: {
				closed: [
					{payload: {parent: 'story', target: 'topPanel', subTarget: 'move', value: 0}, timeout: 100}
				]
			}
		}

		storySpinner(formStates[this.status][this.update])
		this.status = this.update
	}

	render() {

		const { story, classes, app, dimensions, children } = this.props

		const formHome = {
			move : [
				{x: '110px', y: '65px', t: 0.2, tF: 'ease-in-out'},
				{x: '20px', y: '65px', t: 0.2, tF: 'ease-in-out'},
			],
			size: [
				{height: '40px', width: '40px', t: 0.4, tF: 'ease-in-out'},
				{height: '40px', width: dimensions.width - 200, t: 0.4, tF: 'ease-in-out'},
				{height: '40px', width: '250px', t: 0.2, tF: 'ease-in-out'},
			],
		}

		this.update !== null && this.spinEngine()
		this.update = null

		return (
			<Fragment>
				<MovingPaper
					story={formHome.move}
					position={story.topPanel.move}
					dimensions={dimensions}
				>
	        <ExpandingPaper
	        	story={formHome.size}
	        	position={story.topPanel.size}
	        	dimensions={dimensions}
	        >
	          {children}
	        </ExpandingPaper>
	      </MovingPaper>
	    </Fragment>
		)
	}
}

export default TopPanel;