import React, { Component, Fragment } from 'react';
import MovingPaper from '../../../tools/crazyPaper/MovingPaper';
import ExpandingPaper from '../../../tools/crazyPaper/ExpandingPaper';
import Button from '@material-ui/core/Button';

class Visual extends Component {
	
	constructor(props) {
	  super(props);
	  this.update = null
	  this.status = null
	}

	componentDidMount() {
		const { app } = this.props;
		this.status = app.visual;
	}

	shouldComponentUpdate(nextProps) {
		return( this.props !== nextProps
			? true
			: false
		)
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.update !== nextProps.update) {
			this.update = nextProps.update
		}
	}

	spinEngine = () => {

		const { storySpinner, app } = this.props;

		const formStates = {
			closed: {
				open:[
					{payload: {parent: 'story', target: 'visual', subTarget: 'move', value: 2}, timeout: 0},
					{payload: {parent: 'story', target: 'visual', subTarget: 'move', value: 3}, timeout: 400},
					{payload: {parent: 'story', target: 'visual', subTarget: 'size', value: 1}, timeout: 0},
					{payload: {parent: 'story', target: 'visual', subTarget: 'size', value: 2}, timeout: 400},
				],
				hidden: [
					{payload: {parent: 'story', target: 'visual', subTarget: 'move', value: 0}, timeout: 0},
				],
			},
			open: {
				closed: [
					{payload: {parent: 'story', target: 'visual', subTarget: 'move', value: 2}, timeout: 0},
					{payload: {parent: 'story', target: 'visual', subTarget: 'move', value: 1}, timeout: 800},
					{payload: {parent: 'story', target: 'visual', subTarget: 'size', value: 1}, timeout: 0},
					{payload: {parent: 'story', target: 'visual', subTarget: 'size', value: 0}, timeout: 400},
				],
				expanded: [
					{payload: {parent: 'story', target: 'visual', subTarget: 'move', value: 4}, timeout: 0},
					{payload: {parent: 'story', target: 'visual', subTarget: 'size', value: 3}, timeout: 0},
				]
			},
			hidden: {
				closed: [
					{payload: {parent: 'story', target: 'visual', subTarget: 'move', value: 1}, timeout: 50},
				],
			},
			expanded: {
				open: [
					{payload: {parent: 'story', target: 'visual', subTarget: 'move', value: 3}, timeout: 0},
					{payload: {parent: 'story', target: 'visual', subTarget: 'size', value: 2}, timeout: 0},
				]
			}
		}

		storySpinner(formStates[this.status][this.update])
		this.status = this.update
	}

	render() {

		const { story, classes, setter, app, storySpinner, dimensions, children } = this.props;

		const formHome = {
			move : [
				{x: '20px', y: '65px', t: 0.2, tF: 'ease-in-out'},
				{x: '60px', y: '65px', t: 0.2, tF: 'ease-in-out'},
				{x: '39px', y: '60px', t: 0.2, tF: 'ease-in-out'},
				{x: '59px', y: '60px', t: 0.4, tF: 'ease'},
				{x: '239px', y: '60px', t: 0.4, tF: 'ease'},

			],
			size: [
				{height: '40px', width: '40px', t: 0.4, tF: 'ease-in-out'},
				{height: dimensions.height - 60, width: '40px', t: 0.4, tF: 'ease-in-out'},
				{height: dimensions.height - 60, width: dimensions.width - 60, t: 0.4, tF: 'ease'},
				{height: dimensions.height - 60, width: dimensions.width - 240, t: 0.4, tF: 'ease'},
			],
		}

		this.update !== null && this.spinEngine()
		this.update = null

		return (
			<fragment>
				<MovingPaper
					story={formHome.move}
					position={story.visual.move}
					dimensions={dimensions}
				>
	        <ExpandingPaper
	        	story={formHome.size}
	        	position={story.visual.size}
	        	dimensions={dimensions}
	        >
	          {children}
	        </ExpandingPaper>
	      </MovingPaper>
	    </fragment>
		)
	}
}

export default Visual;
