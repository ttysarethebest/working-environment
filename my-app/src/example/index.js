import React, { Component, Fragment } from 'react';
import TopPanel from './shell/topPanel';
import SidePanel from './shell/sidePanel';
import Visual from './shell/visual';
import VisualContents from './shellContents/visual';
import TopPanelContents from './shellContents/topPanel';
import SidePanelContents from './shellContents/sidePanel';
import TopBar from './components/TopBar';
import HomeScreen from './components/HomeScreen';

let env = {
  sideActivationTimer: false,
  sideActivationControl: null,
}

const menuItems = {
  'Home': {
    icon: 'bubble_chart',
    componentName: '/'
  },
  'Dashboard': {
    icon: 'dashboard',
    componentName: '/tableau'
  },
  'Demand Forecaster': {
    icon: 'insert_chart',
    componentName: '/forecaster'
  }
};

class Example extends Component {
  state = {
    app: {
      visual: 'hidden',
      topPanel: 'hidden',
      sidePanel: 'hidden',
      tstat: false,
    },
    story: {
      topPanel: {move: 1, size: 0},
      sidePanel: {move: 0, size: 0},
      visual: {move: 0, size: 0},
    },
  }

  componentDidMount() {
    window.addEventListener("mousemove", this.handleSideActivation);
  }

  componentWillUnmount() {
    window.removeEventListener("mousemove", this.handleSideActivation);
  }


  handleSideActivation = (event) => {

    const { clientX, clientY } = event;
    const { app } = this.state;

    if (clientX < 60 && clientY < 85 && app.sidePanel === 'hidden' && app.tstat === false) {
      this.setter({parent: 'app', target: 'tstat', value: true}, 'sideActivation')
      setTimeout(() => this.setter({parent: 'app', target: 'tstat', value: false}, 'sideActivation'), 800)
      this.setter({parent: 'app', target: 'sidePanel', value: 'closed'}, 'sideActivation')
      this.setter({parent: 'app', target: 'topPanel', value: 'closed'}, 'sideActivation')
      this.setter({parent: 'app', target: 'visual', value: 'closed'}, 'sideActivation')
    }

    else if (app.sidePanel !== 'closed' || app.topPanel !== 'closed' || app.visual !== 'closed' && app.tstat === false) {
      clearTimeout(env.sideActivationControl)
      env.sideActivationTimer = false
    }

    else if (app.sidePanel === 'closed' && app.topPanel === 'closed' && app.visual === 'closed' && env.sideActivationTimer == false && app.tstat === false) {
      env.sideActivationTimer = true
      env.sideActivationControl = setTimeout(() => {
        if (app.sidePanel === 'closed' && app.topPanel === 'closed' && app.visual === 'closed') {
          this.setter({parent: 'app', target: 'tstat', value: true}, 'sideActivation')
          setTimeout(() => this.setter({parent: 'app', target: 'tstat', value: false}, 'sideActivation'), 800)
          this.setter({parent: 'app', target: 'sidePanel', value: 'hidden'}, 'sideActivation')
          this.setter({parent: 'app', target: 'topPanel', value: 'hidden'}, 'sideActivation')
          this.setter({parent: 'app', target: 'visual', value: 'hidden'}, 'sideActivation')
        }
        env.sideActivationTimer = false
      }, 2000)
    }

  }

  storySpinner = (eventArray) => {
    for (let i = 0; i < eventArray.length; i++) {
      setTimeout(() => this.setter(eventArray[i].payload, 'index/storySpinner'), eventArray[i].timeout)
    }
  }

  setter = (event, id) => {
    let state = Object.assign({}, this.state);
    if (event.subTarget)
      state[event.parent][event.target][event.subTarget] = event.value;
    else
      state[event.parent][event.target] = event.value;
    this.setState(state)
    console.log(id)
  }

  render() {

    const { app, story } = this.state;
    const { dimensions } = this.props;

    return (
      <Fragment>
        <div>
          <div style={{zIndex: '10', position: 'absolute'}}>
            <TopPanel
              app={app}
              story={story}
              update={app.topPanel}
              storySpinner={this.storySpinner}
              dimensions={dimensions}
            >
              <TopPanelContents
                app={app}
                setter={this.setter}
              />
            </TopPanel>
            <SidePanel
              app={app}
              story={story}
              update={app.sidePanel}
              storySpinner={this.storySpinner}
              dimensions={dimensions}
            >
              <SidePanelContents
                story={story}
                app={app}
                setter={this.setter}
                menuItems={menuItems}
              />
            </SidePanel>
            <div style={{zIndex: '-10', position: 'absolute'}}>
              <Visual
                app={app}
                story={story}
                update={app.visual}
                storySpinner={this.storySpinner}
                dimensions={dimensions}
              >
                <VisualContents
                  story={story}
                  app={app}
                  setter={this.setter}
                />
              </Visual>
            </div>
          </div>
          <div style={{zIndex: '0'}}>
            <TopBar
              dimensions={dimensions}
            />
            <HomeScreen
              dimensions={dimensions}
            />
          </div>
        </div>
      </Fragment>
    )
  }
};

export default Example;
