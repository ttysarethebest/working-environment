import React, { Component, Fragment } from 'react';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/List';
import Icon from '@material-ui/core/Icon';
import {Link} from 'react-router-dom';
import CompSwitch from '../../../tools/CompSwitch'
import IconButton from '@material-ui/core/IconButton';

const styles = (theme) => {
	const palette = theme.colourPalette
	return ({
		menuContainer: {
			height: '100%',
			width: '100%',
			backgroundColor: palette.highlightedPrimary,
		},
		menuItem: {
			marginBottom: '20px',
			paddingLeft: '17px',
	    color: palette.text,
	    fontSize: '17px',
	    '&:hover': {
	      color: palette.highlightedText,
	      textDecoration: 'underline',
	      boxShadow: '0 1px 1px 1px rgba(0,0,0,0.4)',
	    },
		},
		menuItemSelected: {
			paddingLeft: '29px',
	    color: palette.text,
	    fontSize: '17px',
		},
		panelIcon: {
			color: palette.secondary,
	    marginTop: '7px',
	    marginRight: '17px',
	    fontSize: '26px',
		},
		menuIcon: {
			color: palette.secondary,
		},
		menuIconClosed: {
			height: '40px',
			width: '40px',
		},
		menuOperation: {
			background: palette.highlightedPrimary,
	    boxShadow: '0 0.5px 0.5px 0.5px rgba(0,0,0,0.3)',
	    position: 'absolute',
	    width: '100%',
	    bottom: '0%',
	    '&:hover': {
	      boxShadow: '0 1px 1px 1px rgba(0,0,0,0.4)',
	    },
	  },
	  menuItemClose: {
			paddingLeft: '17px',
	    color: palette.text,
	    fontSize: '17px',
	    '&:hover': {
	      color: palette.highlightedText,
	      textDecoration: 'underline',
	      boxShadow: '0 1px 1px 1px rgba(0,0,0,0.4)',
	    },  
		},
		topLogoInner: {
			paddingLeft: '5px',
			paddingTop: '40px',
			paddingBottom: '30px',
			transition: 'all .5s ease'
		},
		topLogoExpanded: {
			paddingLeft: '92px',
			paddingTop: '40px',
			paddingBottom: '30px',
			transition: 'all .5s ease'
		}
	})
}

class SideNavigation extends Component {


	render() {
		const { classes, menuItems, displayedContent, update, app } = this.props;

		let menuArray = []
    for (let key in menuItems) {
      menuArray.push(key);
    };

		return (
			<Fragment>
				<CompSwitch delay={500} change={update} full>
					{update === true
					?<Fragment>
						<div className={classes.menuContainer} >
							<div className={app.sidePanel !== 'expanded' ? classes.topLogoInner : classes.topLogoExpanded}>
								<IconButton  aria-label="Delete">
					        <Icon classes={{root: classes.menuIcon}}>menu</Icon>
					      </IconButton>
							</div>
			        {menuArray.map((item, index) => {
			          return (
			            <div
			              key={index}
			              className={classes.drawerMenuItem}
			            >
			              <Link to={menuItems[item].componentName} style={{ textDecoration: 'none' }}>
			                <MenuItem
			                  className={
			                    displayedContent !== item
			                      ? classes.menuItem
			                      : classes.menuItemSelected
			                  }
			                >
			                  <ListItemIcon>
			                    <Icon classes={{root: classes.panelIcon}}>
			                      {menuItems[item].icon}
			                    </Icon>
			                  </ListItemIcon>
			                  {item}
			                </MenuItem>
			              </Link>  
			            </div>
			          )
			        })}
			      </div>
			      <div className={classes.menuOperation} >
			        <MenuItem className={classes.menuItemClose}>
			          <CompSwitch delay={300} change={(app.sidePanel === 'expanded')}>
				          <ListItemIcon>
				          	<Icon classes={{root: classes.panelIcon}}>
			                {app.sidePanel !== 'expanded' ? 'chevron_right' : 'chevron_left'}
			              </Icon>
				          </ListItemIcon>
				        </CompSwitch>
			          {'Close Panel'}
			        </MenuItem>
			      </div>
			    </Fragment>
			    :<div>
						<IconButton className={classes.menuIconClosed}>
			        <Icon>menu</Icon>
			      </IconButton>
					</div>
					}
		    </CompSwitch>
		  </Fragment>  
		)
	}
}

export default withStyles(styles)(SideNavigation);