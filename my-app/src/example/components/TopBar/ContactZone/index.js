import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => {
	const palette = theme.colourPalette
	return ({
		text: {
			color: '#FFFFFF',
			textDecoration: 'line-through',
		}
})}

class Contact extends Component {
	render() {

		const { classes } = this.props

		return (
			<div>
				<Typography variant="title" className={classes.text}>
		      +27790799051
				</Typography>
			</div>
		)
	}
}

export default withStyles(styles)(Contact);