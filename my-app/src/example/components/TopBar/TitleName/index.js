import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import './index.css'
import Typography from '@material-ui/core/Typography';

const styles = (theme) => {
	const palette = theme.colourPalette
	return ({
		 text: {
		 	textShadow: '0 0 80px rgba(255,255,255,.6)',
		 	textTransform: 'uppercase',
		 },
})}

class Title extends Component {
	render() {

		const { classes, response } = this.props;

		return (
			<div className={"animate " + classes.text}>
					<Typography variant="display2">
			      {response !== 'mobile' && 'Web_Kraken'}
					</Typography>
			</div>
		)
	}
}

export default withStyles(styles)(Title);