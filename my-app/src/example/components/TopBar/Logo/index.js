import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import srcLogo from './download.jpg'

const styles = (theme) => {
	const palette = theme.colourPalette
	return ({
		outer: {
			width: '65px',
			height: '65px',
		},
})}

const ImgLogo = () => 
  <img
  	style={{borderRadius: '50px'}}
    src={srcLogo}
    width="100%"
    height="100%"
    alt=''
  />

class Logo extends Component {
	render() {

		const { classes, children } = this.props

		return (
			<div className={classes.outer} >
				<ImgLogo />
			</div>
		)
	}
}

export default withStyles(styles)(Logo);