import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Logo from './Logo';
import Title from './TitleName';
import Contact from './ContactZone';

const styles = (theme) => {
	const palette = theme.colourPalette
	return ({
		outerDiv: {
			backgroundColor: palette.primary,
			boxShadow: theme.shadows[3],
		}, 
		title:{
			position: 'absolute',
			left: '120px',
			top: '14px',
		},
		logo: {
			position: 'absolute',
			left: '8px',
			top: '8px',
		},
		contact: {
			position: 'absolute',
			right: '100px',
			top: '15px',
			border: 'dotted',
		},
})}

class TopBar extends Component {

	render() {

		const { classes, dimensions } = this.props;

		return (
			<div
				style={{height: '80px', width: dimensions.width}} 
				className={classes.outerDiv}
			>
				<div className={classes.logo} >
					<Logo/>
				</div>
				<div className={classes.title} >
					<Title
						response={dimensions.userPlat}
					/>
				</div>
				<div className={classes.contact}>
					<Contact />
				</div>
			</div>
		)
	}
}

export default withStyles(styles)(TopBar);