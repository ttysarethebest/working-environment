import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => {
	const palette = theme.colourPalette
	return ({
		outerDiv: {
			backgroundColor: palette.secondary,
		},
		contents: {
			position: 'absolute',
		}
})}

class Contents extends Component {
	render() {
		return (
			<div>
				Contents
			</div>
		)
	}
}

export default withStyles(styles)(Contents)