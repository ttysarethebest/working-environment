import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => {
	const palette = theme.colourPalette
	return ({
		
})}

class VideoBG extends Component {
	render() {

		const { classes, dimensions } = this.props

		const styleInline = {
			height: '100%',
			width: '100%',
			objectFit: 'fill',
		}

		return (
			<video autoPlay loop muted style={styleInline}> 
			  <source src="https://ak2.picdn.net/shutterstock/videos/24102952/preview/stock-footage-abstract-neon-background-luminous-swirling-glowing-spiral-cover-black-elegant-halo-around.webm" type="video/webm" />
			  <source src="https://ak2.picdn.net/shutterstock/videos/24102952/preview/stock-footage-abstract-neon-background-luminous-swirling-glowing-spiral-cover-black-elegant-halo-around.webm" type="video/mp4" />
			  browser not supported
			</video>
		)
	}
}

export default withStyles(styles)(VideoBG);