import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import VideoBG from './VideoBG';
import Contents from './Contents';

const styles = (theme) => {
	const palette = theme.colourPalette
	return ({
		outerDiv: {
			backgroundColor: palette.secondary,
		},
		contents: {
			position: 'absolute',
			backgroundColor: palette.secondary,
		}
})}

class HomeScreen extends Component {
	render() {

		const { classes, dimensions } = this.props;

		const styleInline = {
			video: {
				height: dimensions.height - 60 + 'px',
				width: dimensions.height * 7/4 + 'px',
				marginLeft: (() => {
					let factor = (dimensions.height * 7/4) - dimensions.width;
					if (factor !== 0 )
						return ((factor / 2 * -1) + 'px');
					else
						return('0px')
				})()
			},
			outerDiv: {
				height: dimensions.height -60 + 'px',
				width: dimensions.width + 'px',
				overflowX: 'hidden',
			}
		}

		return (
			<Fragment>
				<div style={styleInline.outerDiv} className={classes.outerDiv}>
					<div style={styleInline.video}>
						<div className={classes.contents}>
							<Contents />
						</div>
						<VideoBG
							dimensions={dimensions}
						/>
					</div>
				</div>
			</Fragment>	
		)
	}
}

export default withStyles(styles)(HomeScreen);
